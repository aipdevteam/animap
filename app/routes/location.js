import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return this.store.find('node', {'node_list': params.location_id});
  }
});

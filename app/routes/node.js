import Ember from 'ember';

export default Ember.Route.extend({
  model: function(params) {
    return this.store.findRecord('node', params.node_id);
  },

  setupController: function(controller, model){
    this._super(controller, model);
    model.set('isOpen', true);

    let app_controller = this.controllerFor('application');
    app_controller.panMap(model);
  },

  actions: {
  }
});

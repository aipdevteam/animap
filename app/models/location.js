import DS from 'ember-data';
import MF from 'model-fragments';

export default DS.Model.extend({
	name: DS.attr('string'),
	parent: DS.attr('boolean'),
	geom: MF.fragment('geom')
});

import Ember from 'ember';
import DS from 'ember-data';
import MF from 'model-fragments';

export default DS.Model.extend({
  name: DS.attr('string'),
  parent: DS.belongsTo('node', { inverse: 'children' }),
  children: DS.hasMany('node', { inverse: 'parent' }),
  root: DS.belongsTo('node', { inverse: 'node_list'}),
  node_list: DS.hasMany('node', {inverse: 'root'}),
  passport_type: DS.attr('string'),
  id_passport_type: DS.attr('number'),
  id_element: DS.attr('number'),
  geom: MF.fragment('geom'),
  conns_1: DS.hasMany('connection', {inverse: 'id_wezel_1'}),
  conns_2: DS.hasMany('connection', {inverse: 'id_wezel_2'}),

  // selected: false,
  isOpen: false,

  coords: Ember.computed( function() {
    if (this.get('geom')) {
      if (this.get('geom').get('coordinates')) {
        return this.get('geom').get('coordinates');
    }}
    else {
      return [null, null];
    }
  }),
  lat: Ember.computed( function() {
    return this.get('coords').objectAt(1);
  }),
  lng: Ember.computed( function() {
    return this.get('coords').objectAt(0);
  }),
});

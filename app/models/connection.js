import DS from 'ember-data';

export default DS.Model.extend({
  // "id", "element_1", "element_2", "id_wezel_1", "id_wezel_2",
  // "id_port_1", "id_port_2", "id_gniazdo_1", "id_gniazdo_2", "typ",
  element_1: DS.attr('string'),
  element_2: DS.attr('string'),
  nr_1: DS.attr('string'),
  nr_2: DS.attr('string'),
  id_wezel_1: DS.belongsTo('node', {inverse: 'conns_1'}),
  id_wezel_2: DS.belongsTo('node', {inverse: 'conns_1'}),

});

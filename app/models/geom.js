import Ember from 'ember';
import DS from 'ember-data';
import MF from 'model-fragments';

export default MF.Fragment.extend({
  type: DS.attr('string'),
  coordinates: MF.array(),

  lat: Ember.computed('coordinates', function() {
    return `${this.get('coordinates')[1]}`;
  }),
  lng: Ember.computed('coordinates', function() {
    return `${this.get('coordinates')[0]}`;
  })
});

import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['q'],
  q: null,
  mapZoom: 11,
  mapMaxZoom: 18,
  mapLat: 50.341,
  mapLng: 19.62,

  panMap(node) {
    this.set('mapZoom', this.get('mapMaxZoom'));
    this.set('mapLat', node.get('lat'));
    this.set('mapLng', node.get('lng'));
    // this.model = this.store.findRecord('node', node.get('id'));
  },

  actions: {
    doSearch(param) {
      this.set('q', param);
    },
  }
});

// import Ember from 'ember';
import MarkerClusterLayer from 'ember-leaflet-marker-cluster/components/marker-cluster-layer';

export default MarkerClusterLayer.extend({

  // disableClusteringAtZoom: null,

  options: {
    disableClusteringAtZoom: 18,
  },

  layerSetup: function() {
    // return this.L.markerClusterGroup(...get(this,'requiredOptions'), get(this,'options'));
    this._super(...arguments);
    // console.log(this._layer);
  },

  // zoomToShowLayer: function(target) {
  //   target.__parent.spiderfy();
  //   target.openPopup();
  // },
});

/* jshint node: true */

import Ember from 'ember';
// import ENV from 'anim/config/environment';

export default Ember.Component.extend({
  url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
  lat: 50.341,
  lng: 19.62,
  zoom: 11,
  maxZoom: 18,
  map: this,

  locations: null,

  actions: {
    zoomToMax: function() {
      this.set('zoom', this.get('maxZoom'));
    },
  },

  // this.on('popupopen', function(e) {
  //   var marker = e.popup._source;
  // });
});

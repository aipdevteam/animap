import Ember from 'ember';

export default Ember.Component.extend({
  q: null,

  actions: {
    submitSearch(param) {
      this.sendAction('action', param);
    }
  }
});

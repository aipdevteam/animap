import Ember from 'ember';
import MarkerLayer from 'ember-leaflet/components/marker-layer';

export default MarkerLayer.extend({

  isOpenWatch: Ember.observer('popupOpen', function() {
    console.log('observer is called', this.get('popupOpen'));
    if (! this.get('popupOpen')) {
      this.set('popupOpen', true);
    }
  }),
});

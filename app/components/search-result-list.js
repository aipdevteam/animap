import Ember from 'ember';

export default Ember.Component.extend({

  actions: {
    clickResult: function(param) {
      // console.log(param);
      this.sendAction('action', param);
    },
  },
});

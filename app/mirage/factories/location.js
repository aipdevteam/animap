import Mirage, {faker} from 'ember-cli-mirage';

export default Mirage.Factory.extend({
  name() {
    return faker.address.city();
  },
  parent() {
    return null;
  },
  geom() {
    return {
      type: "Point",
      coordinates: [faker.address.latitude(), faker.address.longitude()]
    };
  }
});

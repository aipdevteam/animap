export default {
  "count": 4889,
  "next": "http://a1.aip.pl:8245/api/locations/?page=2",
  "previous": null,
  "results": [{
    "id": 48771,
    "name": "Chełm, <Chełm>",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7701692, 50.3678956]
    }
  }, {
    "id": 40940,
    "name": "Chełm, <Chełm> 98",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.755478, 50.3596618]
    }
  }, {
    "id": 38629,
    "name": "Chełm, Grabie 18",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7669778, 50.3671722]
    }
  }, {
    "id": 38622,
    "name": "Chełm, Grabie 20",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7664106, 50.367346]
    }
  }, {
    "id": 38632,
    "name": "Chełm, Grabie 21",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.767293, 50.3668284]
    }
  }, {
    "id": 38619,
    "name": "Chełm, Grabie 22",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.766326, 50.3671448]
    }
  }, {
    "id": 38644,
    "name": "Chełm, Grabie 24",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.765618, 50.3675256]
    }
  }, {
    "id": 38635,
    "name": "Chełm, Grabie 27",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7656918, 50.366753]
    }
  }, {
    "id": 38641,
    "name": "Chełm, Grabie 28",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7653108, 50.367204]
    }
  }, {
    "id": 38638,
    "name": "Chełm, Grabie 29",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7652854, 50.366896]
    }
  }, {
    "id": 38681,
    "name": "Chełm, Grabie 30",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7650976, 50.3674178]
    }
  }, {
    "id": 38647,
    "name": "Chełm, Grabie 31",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7652318, 50.3674622]
    }
  }, {
    "id": 38625,
    "name": "Chełm, Grabie bn",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.766754, 50.367185]
    }
  }, {
    "id": 38670,
    "name": "Chełm, Grabie bn",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.7679568, 50.3668738]
    }
  }, {
    "id": 38364,
    "name": "DO WYJEBKI",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5381064, 50.2880196]
    }
  }, {
    "id": 40157,
    "name": "Jaroszowiec, ul. Kolejowa",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.596549, 50.3342168]
    }
  }, {
    "id": 40156,
    "name": "Jaroszowiec, ul. Kolejowa",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5972758, 50.3350814]
    }
  }, {
    "id": 40184,
    "name": "Jaroszowiec, ul. Kolejowa",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6003872, 50.335821]
    }
  }, {
    "id": 40165,
    "name": "Jaroszowiec, ul. Kolejowa 1",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.597595, 50.3360504]
    }
  }, {
    "id": 40151,
    "name": "Jaroszowiec, ul. Kolejowa 10",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6009022, 50.3361412]
    }
  }, {
    "id": 40153,
    "name": "Jaroszowiec, ul. Kolejowa 11",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.600508, 50.3360932]
    }
  }, {
    "id": 40182,
    "name": "Jaroszowiec, ul. Kolejowa 13",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5994404, 50.3357954]
    }
  }, {
    "id": 40495,
    "name": "Jaroszowiec, ul. Kolejowa 14",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5990648, 50.3358296]
    }
  }, {
    "id": 40155,
    "name": "Jaroszowiec, ul. Kolejowa 14a",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5973202, 50.3352808]
    }
  }, {
    "id": 40159,
    "name": "Jaroszowiec, ul. Kolejowa 15",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5988422, 50.3345242]
    }
  }, {
    "id": 40163,
    "name": "Jaroszowiec, ul. Kolejowa 15a",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.599671, 50.3342442]
    }
  }, {
    "id": 40161,
    "name": "Jaroszowiec, ul. Kolejowa 19",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5959474, 50.335441]
    }
  }, {
    "id": 46874,
    "name": "Jaroszowiec, ul. Kolejowa 1a",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6138828, 50.3440362]
    }
  }, {
    "id": 38472,
    "name": "Jaroszowiec, ul. Kolejowa 2",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5998454, 50.335542]
    }
  }, {
    "id": 37293,
    "name": "Jaroszowiec, ul. Kolejowa , blok 3",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6030152, 50.3365778]
    }
  }, {
    "id": 37294,
    "name": "Jaroszowiec, ul. Kolejowa , blok 4",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6023666, 50.336841]
    }
  }, {
    "id": 37295,
    "name": "Jaroszowiec, ul. Kolejowa , blok 5",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6023124, 50.337289]
    }
  }, {
    "id": 37296,
    "name": "Jaroszowiec, ul. Kolejowa , blok 6",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6022312, 50.3376118]
    }
  }, {
    "id": 37297,
    "name": "Jaroszowiec, ul. Kolejowa , blok 9",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6014548, 50.3362354]
    }
  }, {
    "id": 40089,
    "name": "Jaroszowiec, ul. Leśna",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.601141, 50.3377402]
    }
  }, {
    "id": 40131,
    "name": "Jaroszowiec, ul. Leśna",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5970518, 50.3359478]
    }
  }, {
    "id": 40130,
    "name": "Jaroszowiec, ul. Leśna",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5963774, 50.336624]
    }
  }, {
    "id": 37289,
    "name": "Jaroszowiec, ul. Leśna , blok 2",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6010644, 50.3370708]
    }
  }, {
    "id": 37393,
    "name": "Jaroszowiec, ul. Leśna , blok 24",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5963318, 50.3360148]
    }
  }, {
    "id": 37401,
    "name": "Jaroszowiec, ul. Leśna , blok 25",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5959888, 50.3357256]
    }
  }, {
    "id": 37400,
    "name": "Jaroszowiec, ul. Leśna , blok 26",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5955656, 50.3356756]
    }
  }, {
    "id": 37394,
    "name": "Jaroszowiec, ul. Leśna , blok 27",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5959728, 50.335945]
    }
  }, {
    "id": 37392,
    "name": "Jaroszowiec, ul. Leśna , blok 28",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5960384, 50.3363062]
    }
  }, {
    "id": 37391,
    "name": "Jaroszowiec, ul. Leśna , blok 29",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5959338, 50.336553]
    }
  }, {
    "id": 37290,
    "name": "Jaroszowiec, ul. Leśna , blok 3",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.6011798, 50.3368122]
    }
  }, {
    "id": 37390,
    "name": "Jaroszowiec, ul. Leśna , blok 30",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5957238, 50.336435]
    }
  }, {
    "id": 37389,
    "name": "Jaroszowiec, ul. Leśna , blok 31",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5949792, 50.3364274]
    }
  }, {
    "id": 37395,
    "name": "Jaroszowiec, ul. Leśna , blok 32",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.595369, 50.3360732]
    }
  }, {
    "id": 37396,
    "name": "Jaroszowiec, ul. Leśna , blok 33",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5947524, 50.3360778]
    }
  }, {
    "id": 37397,
    "name": "Jaroszowiec, ul. Leśna , blok 34",
    "parent": null,
    "geom": {
      "type": "Point",
      "coordinates": [19.5946216, 50.3358158]
    }
  }]
};

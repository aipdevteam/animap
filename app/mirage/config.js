import Ember from 'ember';
// import {faker} from 'ember-cli-mirage';

export default function() {
  this.urlPrefix = 'http://localhost:8000';
  this.namespace = 'api';
  this.get('/locations', (db, request) => {
    const q = request.queryParams.q;
    if (Ember.isEmpty(q)) {
      return {
        "count": db.locations.length,
        // "next": "http://a1.aip.pl:8245/api/locations/?page=2",
        // "previous": null,
        "results": db.locations
        };
    }
    return db.locations.filter(item => item.name.match(q));
  });
}
